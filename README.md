### Decrypt Extension Mini
> 网站解密插件迷你版

#### 使用方式
 1. 拉取代码(打包下载)
 2. chrome添加插件



#### 代码结构
```
.
├── css 样式文件
│   ├── decrypt.css           devtools页面插件样式
│   └── Highlight.css         json高亮眼样式
├── DecryptMini.html           解密页面
├── devtools.html              解密页面注入devtools入口
├── image                      icon
│   └── icon.png
├── js
│   ├── background.js         
│   ├── decrypt.js            解密逻辑
│   └── devtools.js           解密页面注入devtools入口
├── lib                        依赖
│   ├── bootstrap.min.css
│   ├── bootstrap.min.js
│   ├── jquery-3.4.1.min.js
│   ├── jquery-tooltipify.min.js
│   └── tooltipify.min.css
├── manifest.json               权限清单文件
├── popup.html
└── README.md
```


#### 参考文档
```
Chrome插件(扩展)开发全攻略
https://www.cnblogs.com/liuxianan/p/chrome-plugin-develop.html

```
