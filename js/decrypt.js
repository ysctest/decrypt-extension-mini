/*
  auth: ysc
 */

// 默认解密接口url
var decryptUrlDefault = "http://127.0.0.1:8002/decrypt"
var decryptUrl        = ""

var apis = [];

// 是否展示域名
var isShowDomain = false;

$(document).ready(domInit);

function domInit() {
    /*
      不能直接在dom上绑定事件,通过初始化jquery绑定相应的操作事件
     */
    $("#clearRequestList").on("click",function(){
            apis = []
            $(".list-group-item").remove();
            document.getElementById("requestDetail").style.display="none";
            document.getElementById("tools_info").style.display="block";
        }
    )
    $("#requestListUrlIsShowDomain").on("click",function () {
        isShowDomain = !isShowDomain
    })
    getDecryptUrlStrongInit()

    $(".btn-pre-copy").on("click",function(){preCopy(this)})
    $("#decryptUrlInput").on("blur",function(){
        let value = $("#decryptUrlInput").val()
        console.log(value)
        setDecryptUrlStrong(value)
    })

    startNetWork()
}


function getDecryptUrlStrongInit() {
    /*
      使用chrome.storage(可随账号同步) 读取解密接口url,不存在读取 默认解密url
     */
    chrome.storage.sync.get(['decryptUrl'], function(result) {
        let strongDecryptUrl = result.decryptUrl
        if (strongDecryptUrl !== undefined && strongDecryptUrl !== ''){
            decryptUrl = strongDecryptUrl
        }else {
            decryptUrl = decryptUrlDefault
        }
        $("#decryptUrlInput").attr("value",decryptUrl)
    });
}

function setDecryptUrlStrong(value) {
    /*
      写入解密接口url 至  chrome.storage
     */
    chrome.storage.sync.set({"decryptUrl": value}, function() {
        getDecryptUrlStrongInit()
    });
}

function startNetWork(){
    /*
      使用 chrome extension api 获取api 并调用解密接口
     */
    chrome.devtools.network.onRequestFinished.addListener(
        async (...args) => {


            try {
                const [{
                    // 请求的类型，查询参数，以及url
                    request,
                    response,
                    // 该方法可用于获取响应体
                    getContent,
                }] = args;
                // todo 这个地方做域名判断
                if (request.url.indexOf("127.0.0.1") != -1){
                    request.content = await new Promise((res, rej) => getContent(res));
                    console.log("这是request",request)
                    if (request.postData && request.postData.text.length >= 0) {
                        let param = new Object();
                        let requestBody = JSON.parse(request.postData.text)
                        let responseBody = request.content
                        console.log(requestBody, responseBody)

                        param['req'] = requestBody['req']
                        param['res'] = responseBody
                        request.responseHeader = response.headers
                        console.log(param)
                        console.log(decryptUrl)
                        $.ajax({
                            url: decryptUrl,
                            async: true,
                            type: "POST",
                            data: JSON.stringify(param),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (res) {
                                console.log("解密接口RES",res)
                                try{
                                    request["decryptRequestData"] = res.req
                                    request["decryptResponseData"] =  res.res
                                    writeUrlToList(request);
                                }catch(err){
                                    console.log(" *********************** START *********************** ")
                                    console.log("Error Message: ")
                                    console.log(err)
                                    console.log("ResponseDate: ")
                                    console.log(res)
                                    console.log(" *********************** END *********************** ")
                                }
                            },
                            error: function (err) {
                                console.log(err)
                            }
                        })
                    }
                }


            } catch (err) {
                console.log(err.stack || err.toString());
            }

        }
    );
}

function writeUrlToList(obj) {
    /*
       写入 url list
     */

    console.log(obj)
    apis.push(obj)
    let objIndex = apis.indexOf(obj)

    let span =  document.createElement("span");
    $(span).tooltipify({content:obj.url})

    let spanHtml = '';
    spanHtml = obj.url

    span.innerHTML = spanHtml



    let li =  document.createElement("li");
    li.className = "list-group-item"
    // li.style.overflow='auto';
    li.id = objIndex
    li.append(span)
    console.log(obj)
    // $(span).tooltipify(
    //     {content:""}
    // )
    $(".list-group").append(
        li
    )
    $("#"+objIndex).on("click",function(){
        document.getElementById("requestDetail").style.display="block";
        document.getElementById("tools_info").style.display="none";
        $(".active").attr("class","list-group-item")
        this.className = "list-group-item active "
        console.log(apis[li.id])
        // $(".well.well-lg").remove();
        writeApiDetails(apis[li.id])
    })
}

function writeApiDetails(obj) {
    /*
      * 写入数据详情页
     */
    let decryptRequestData = serializersJson(obj.decryptRequestData)
    let decryptResponseData = serializersJson(obj.decryptResponseData)
    let reqBodyJson = serializersParamsToJson(obj.postData.params)

    let reqHeadText = headObjToText(obj.headers)
    let resHeadText = headObjToText(obj.responseHeader)

    // 点击url,写入页面数据
    $("#reqUrl").html(obj.url)
    $("#reqMethod").html(obj.method)


    $("#decryptReqBody").html(decryptRequestData)
    $("#decryptResBody").html(decryptResponseData)
    $("#reqBody").html(obj.postData.text)
    $("#reqBodyJson").html(reqBodyJson)
    $("#resBody").html(obj.content)
    $("#reqHeader").html(reqHeadText)
    $("#resHeader").html(resHeadText)

}

function syntaxHighlight(json) {
    /**
     * 配合 Highlight.css 生成pre json高亮标签
     * @type {string}
     */
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        let cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


function serializersJson(jsonStr) {
    /*
    字符串json格式化并高亮
     */
    let str;
    try {
        // 当 字典值 中包含 [] 时 JSON.parse 系列存在问题，导致无法高亮
        str = syntaxHighlight(JSON.stringify(JSON.parse(jsonStr),null,4))
    }catch (e) {
        str = jsonStr
        console.log(e.stack || e.toString());
    }
    return str;
}


function serializersParamsToJson(params) {
    /*
    params 格式化为 json
     */
    let jsonStr = {}
    try {
        for (let param in params){
            jsonStr[params[param].name] = params[param].value
        }
    }catch (e) {
        console.log("serializersParamsToJson error")
        console.log(e.stack || e.toString());
        console.log(params)
    }
    return syntaxHighlight(JSON.stringify(jsonStr,null,4));

}

function headObjToText(headers) {
    let headText = '';
    for (let headerIndex in headers){
        headText = headText + "\n" + headers[headerIndex].name + ":" + headers[headerIndex].value
    }
    return headText;

}

function preCopy(obj) {
    /**
    * 执行复制代码操作
    * @param obj
    */

    let btn = $(obj);
    // let pre = btn.parent();
    // let pre = btn.parent().first();
    let pre = btn.siblings("pre");
    let temp = $("<textarea></textarea>");
    btn.text("");
    temp.text(pre.text());
    temp.appendTo(pre);
    temp.select();
    document.execCommand('copy');
    temp.remove();
    btn.text("复制成功");
    setTimeout(()=> {
        btn.text("复制代码");
    },1500);
}
